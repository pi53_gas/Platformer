﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SFML.Graphics;
namespace MainMenu
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            
                Level level = new Level();
                level.Owner = Application.Current.MainWindow;
                level.Show();
                
            

        }

        private void Options_Click(object sender, RoutedEventArgs e)
        {
            Options options = new Options();
            options.Owner = this;
            options.Show();
        }

        private void ScoreList_Click(object sender, RoutedEventArgs e)
        {
            User user = new MainMenu.User();
            user.Owner = this;
            user.Show();
        }
    }
}
