﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
namespace Creation
{
    class Version
    {
        public Font font = new Font("Sail-Regular.otf");
        public Text text;
        public Font FontDecor = new Font("AlexBrush-Regular.ttf");
        public Text TextDecor;
        public Font FontEnd = new Font("blackjack.otf");
        public Text TextEnd;
        public Font FontHealth = new Font("GreatVibes-Regular.otf");
        public Text TextHealth;
       
        

        public void Formatting()
        {
            text = new Text("", font, 20);
            text.Color = new Color(Color.Red);
            TextDecor = new Text("", FontDecor, 25);
            TextDecor.Color = new Color(Color.Yellow);
            TextEnd = new Text("", FontEnd, 30);
            TextEnd.Color = new Color(Color.Magenta);
            TextHealth = new Text("", font, 25);
            TextHealth.Color = new Color(Color.Green);

        }
       
        public void Word(RenderWindow window, MainPersonage personage, View view, float gameTime, Score score)
        {
            TextDecor.DisplayedString = String.Format("Coin: " + score.Coin + "\n" + "Bonus: " + score.Bonus + "\n" + "Ghost: " + score.Ghost + "\n" + "Moon: " + score.Moon + "\n" + "Pumpkin: " + score.Pumpkin);
            TextDecor.Position = new Vector2f(view.Center.X + 250, view.Center.Y + 120);
            window.Draw(TextDecor);

            if (personage.Life == false)
            {
                text.CharacterSize = 40;
                text.DisplayedString = String.Format("Game over.");
                text.Position = new Vector2f(view.Center.X - 100, view.Center.Y - 100);
                window.Draw(text);
            }
            else
            {
                
                TextHealth.DisplayedString = String.Format("Health: " + personage.Health + "\nTime : " + (int)gameTime + " s");
                TextHealth.Position = new Vector2f(view.Center.X - 400, view.Center.Y - 300);
                window.Draw(TextHealth);
            }

            if(personage.IsTouch ==true && personage.Life == false)
            {
                TextEnd.DisplayedString = String.Format("Thank you. You've done it. You've founded my friend.\n Now, you may close the window.");
                TextEnd.Position = new Vector2f(view.Center.X - 300, view.Center.Y - 200);
                window.Draw(TextEnd);
            }

            if (personage.Life == false && personage.IsTime == true)
            {
                text.CharacterSize = 40;
                text.DisplayedString = String.Format("Game over. Time is up");
                text.Position = new Vector2f(view.Center.X - 100, view.Center.Y - 200);
                window.Draw(text);
            }
           
        }
    }
}
