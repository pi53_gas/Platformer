﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
namespace Creation
{
    class Scrolling
    {
        public void Scroll(float X, float Y, View view)
        {
            float TempX = X;
            float TempY = Y;
            if (X < 500)
                TempX = 500;
            if (Y > 945)
                TempY = 945;
            if (X > 3150)
                TempX = 3150;
            view.Center = new Vector2f(TempX, TempY);
        }
        
    }
}
