﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
namespace Creation
{
    class Decoration
    {
        public Texture texture;
        public Sprite sprite;
        public float X { protected set; get; }
        public float Y { protected set; get; }
        public Decoration(float X, float Y, String FileName)
        {
            this.X = X;
            this.Y = Y;
            texture = new Texture(FileName);
            sprite = new Sprite(texture);
            sprite.Position = new Vector2f(X, Y);
        }
        public virtual void Draw(RenderWindow window)
        {
            window.Draw(sprite);
        }
    }
}
