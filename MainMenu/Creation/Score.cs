﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creation
{
   public class Score
    {
        public int Bonus { set; get; }
        public int Coin { set; get; }
        public int Pumpkin { set; get; }
        public int Moon { set; get; }
        public int Ghost { set; get; }
        public Score()
        {
            this.Bonus = 0;
            this.Coin = 0;
            this.Pumpkin = 0;
            this.Moon = 0;
            this.Ghost = 0;
        }
    }
}
