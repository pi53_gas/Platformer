﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
namespace Creation
{
    public class Window
    {
        Soundtrack soundtrack = new Soundtrack();
        public Texture texture; 
        public Sprite sprite;
        protected RenderWindow window;
        Roster roster = new Roster();
        public MainPersonage personage;
        Version version = new Version();
        Clock clock = new Clock();
        Adventure adventure = new Adventure();
        Files file;
        Scrolling scrill = new Scrolling();
        View view;
        Score score = new Score();
        public Clock gameTime = new Clock();
        public void AddToList()
        {
            roster.AddToListEnemies();
            roster.AddToListGhost();
            roster.AddToCoin();
            roster.AddToListHeart();
            roster.AddToListMoon();
            roster.AddToPumpkin();
            roster.AddToListHanako();
        }

        public void ReturnFromList()
        {
            roster.Decor(window);
            roster.Gh(window);
            roster.Cn(window);
            roster.Hr(window);
            roster.Mn(window);
            roster.Pm(window);
            roster.DrawHanako(window);
            roster.Take(personage,score);
            roster.Enemies(personage);
        }
        public void Level1(uint Width, uint Height, string Text, Color color)
        {
            texture =  new Texture("BG.png");
            personage = new MainPersonage(0, 0, 90, 1027, 32, 48, "Sam", "imgonline-com-ua-convertWJPxTr7fIhw2.png");
            soundtrack.MakeSoundtrack(personage);
            AddToList();
            roster.AddToListDecoration();
            window = new RenderWindow(new VideoMode(Width, Height), Text, Styles.Default);
            window.SetVerticalSyncEnabled(true);//Вертикальная синхронизация
            window.SetFramerateLimit(40);//Количество кадров
            view = new View(new Vector2f(0, 0), new Vector2f(Width, Height));

            file = new Files("M.txt");
            version.Formatting();
            window.Closed += (sender, arg) => window.Close();//Событие закрытия окна
            window.Resized += Window_Resized;
            sprite = new Sprite(texture);

        
            while (window.IsOpen)
            {
                window.DispatchEvents();
                float time = clock.ElapsedTime.AsMicroseconds();
                clock.Restart();
                time = time / 800;
                float game = 0;
                game = gameTime.ElapsedTime.AsSeconds();
                clock.Restart();

                if (personage.IsSpell == true)
                {
                    personage.IsSpell = false;
                    roster.spell.Add(new Spell(0, 0, personage.X, personage.Y, 32, 32, "Spell", "600px-SR-icon-spell-Illusion_Dark.png", personage.State));

                }
                scrill.Scroll(personage.sprite.Position.X, personage.sprite.Position.Y, view);

                sprite.Position = new Vector2f(view.Center.X - 900, view.Center.Y - 600);
                window.SetView(view);
                window.Clear(color);
                window.Draw(sprite);
                ReturnFromList();
                
                adventure.Something(window, file, "..\\Content\\");
                personage.Update(time, file);

              
                roster.S(time, file, window);
               
                roster.DrawEnemies(time, file, window);


             
                version.Word(window, personage, view, game, score);
                personage.Draw(window);
                if(Keyboard.IsKeyPressed(Keyboard.Key.Escape))
                {
                    window.Close();
                    soundtrack.music.Stop();
                }
                
               
               
                window.Display();

            }
            file.SaveToFile(personage,score);
            soundtrack.music.Stop();
            


        }

        private void Window_Resized(object sender, SizeEventArgs e)
        {
            window.SetView(new View(new FloatRect(0, 0, e.Width, e.Height)));
        
        }

        public void Level2(uint Width, uint Height, string Text, Color color)
        {
            texture = new Texture("BG2.png");
            roster.AddToLevel2();
            AddToList();
            personage = new MainPersonage(0, 0, 90, 1027, 32, 48, "Sam", "imgonline-com-ua-convertWJPxTr7fIhw2.png");
            soundtrack.MakeSoundtrack(personage);
            window = new RenderWindow(new VideoMode(Width, Height), Text, Styles.Default);
            window.SetVerticalSyncEnabled(true);//Вертикальная синхронизация
            window.SetFramerateLimit(40);//Количество кадров
            view = new View(new Vector2f(0, 0), new Vector2f(Width, Height));
            version.Formatting();
            window.Closed += (sender, arg) => window.Close();//Событие закрытия окна
            window.Resized += Window_Resized;
            sprite = new Sprite(texture);
            file = new Files("Мир.txt");
            roster.AddToLevel2();    
            
            while (window.IsOpen)          
            {
                window.DispatchEvents();
                float time = clock.ElapsedTime.AsMicroseconds();
                clock.Restart();
                time = time / 800;

                
                float game = 0;
                game = gameTime.ElapsedTime.AsSeconds();
                clock.Restart();
                if(game>=120)
                {
                    personage.Life = false;
                    personage.IsTime = true;
                    soundtrack.music.Stop();
                }

                if (personage.IsSpell == true)
                {
                    personage.IsSpell = false;
                    roster.spell.Add(new Spell(0, 0, personage.X, personage.Y, 32, 32, "Spell", "600px-SR-icon-spell-Illusion_Dark.png", personage.State));
                }         

                scrill.Scroll(personage.sprite.Position.X, personage.sprite.Position.Y, view);
                sprite.Position = new Vector2f(view.Center.X - 900, view.Center.Y - 600);
                window.SetView(view);
                window.Clear(color);
                window.Draw(sprite);
                adventure.Something(window, file, "..\\Content2\\");
                ReturnFromList();
                roster.S(time, file, window);
                roster.DrawEnemies(time, file, window);
                personage.Update(time, file);
                version.Word(window, personage, view,game,score);
                if (Keyboard.IsKeyPressed(Keyboard.Key.Escape))
                {
                    window.Close();
                    soundtrack.music.Stop();
                }

                personage.Draw(window);
                window.Display();
               

            }
            file.SaveToFile(personage,score);
            soundtrack.music.Stop();

        }


    }
}
