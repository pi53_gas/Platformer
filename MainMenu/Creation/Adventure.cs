﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
namespace Creation
{
    class Adventure
    {
        public List<Texture> texture = new List<Texture>();
        public List<Sprite> sprite = new List<Sprite>();



       // public string Cont = "..\\Content\\";
        public void Something(RenderWindow window, Files file, String Cont)
        {
            file.Get();
            texture.Add(new Texture(Cont + "Tile (1).png"));
            sprite.Add(new Sprite(texture[0]));
            texture.Add(new Texture(Cont + "Tile (2).png"));
            sprite.Add(new Sprite(texture[1]));
            texture.Add(new Texture(Cont + "Tile (3).png"));
            sprite.Add(new Sprite(texture[2]));
            texture.Add(new Texture(Cont + "Tile (4).png"));
            sprite.Add(new Sprite(texture[3]));
            texture.Add(new Texture(Cont + "Tile (5).png"));
            sprite.Add(new Sprite(texture[4]));
            texture.Add(new Texture(Cont + "Tile (6).png"));
            sprite.Add(new Sprite(texture[5]));
            texture.Add(new Texture(Cont + "Tile (7).png"));
            sprite.Add(new Sprite(texture[6]));
            texture.Add(new Texture(Cont + "Tile (8).png"));
            sprite.Add(new Sprite(texture[7]));
            texture.Add(new Texture(Cont + "Tile (9).png"));
            sprite.Add(new Sprite(texture[8]));
            texture.Add(new Texture(Cont + "Tile (10).png"));
            sprite.Add(new Sprite(texture[9]));
            texture.Add(new Texture(Cont + "Tile (11).png"));
            sprite.Add(new Sprite(texture[10]));
            texture.Add(new Texture(Cont + "Tile (12).png"));
            sprite.Add(new Sprite(texture[11]));
            texture.Add(new Texture(Cont + "Tile (13).png"));
            sprite.Add(new Sprite(texture[12]));
            texture.Add(new Texture(Cont + "Tile (14).png"));
            sprite.Add(new Sprite(texture[13]));
            texture.Add(new Texture(Cont + "Tile (15).png"));
            sprite.Add(new Sprite(texture[14]));
            texture.Add(new Texture(Cont + "Tile (16).png"));
            sprite.Add(new Sprite(texture[15]));
            texture.Add(new Texture(Cont + "bones.png"));
            sprite.Add(new Sprite(texture[16]));
            texture.Add(new Texture(Cont + "R.png"));
            sprite.Add(new Sprite(texture[17]));
            texture.Add(new Texture(Cont + "brick_dark1.png"));
            sprite.Add(new Sprite(texture[18]));
            texture.Add(new Texture(Cont + "TombStone (1).png"));
            sprite.Add(new Sprite(texture[19]));
            texture.Add(new Texture(Cont + "ArrowSign.png"));
            sprite.Add(new Sprite(texture[20]));
            texture.Add(new Texture(Cont + "houseDarkAlt.png"));
            sprite.Add(new Sprite(texture[21]));
            texture.Add(new Texture(Cont + "houseDarkAlt.png"));
            sprite.Add(new Sprite(texture[22]));
            texture.Add(new Texture(Cont + "Crate.png"));
            sprite.Add(new Sprite(texture[23]));


            for (int y = 0; y < file.mass.GetLength(0); y++)
            {
                for (int x = 0; x < file.mass.GetLength(1); x++)
                {

                    if (file.mass[y, x] == '#')
                    {

                        continue;
                    }
                    if (file.mass[y, x] == '0')
                    {

                        sprite[9].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[9]);

                    }
                    if (file.mass[y, x] == '5')
                    {

                        sprite[4].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[4]);

                    }
                    if (file.mass[y, x] == '2')
                    {

                        sprite[1].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[1]);

                    }
                    if (file.mass[y, x] == '3')
                    {

                        sprite[2].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[2]);

                    }
                    if (file.mass[y, x] == '6')
                    {

                        sprite[5].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[5]);

                    }
                    if (file.mass[y, x] == 'B')
                    {

                        sprite[16].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[16]);

                    }
                    if (file.mass[y, x] == 'R')
                    {

                        sprite[17].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[17]);

                    }
                    if (file.mass[y, x] == 's')
                    {

                        sprite[13].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[13]);

                    }
                    if (file.mass[y, x] == 'm')
                    {

                        sprite[14].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[14]);

                    }
                    if (file.mass[y, x] == 't')
                    {

                        sprite[15].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[15]);

                    }
                    if (file.mass[y, x] == '4')
                    {

                        sprite[3].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[3]);

                    }
                    if (file.mass[y, x] == '1')
                    {

                        sprite[0].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[0]);

                    }
                    if (file.mass[y, x] == '7')
                    {

                        sprite[6].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[6]);

                    }
                    if (file.mass[y, x] == '8')
                    {

                        sprite[7].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[7]);

                    }
                    if (file.mass[y, x] == 'O')
                    {

                        sprite[10].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[10]);

                    }
                    if (file.mass[y, x] == 'y')
                    {

                        sprite[11].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[11]);

                    }
                    if (file.mass[y, x] == '9')
                    {

                        sprite[8].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[8]);

                    }
                    if (file.mass[y, x] == 'x')
                    {

                        sprite[12].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[12]);

                    }

                    if (file.mass[y, x] == 'j')
                    {

                        sprite[18].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[18]);

                    }
                    if (file.mass[y, x] == 'G')
                    {

                        sprite[19].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[19]);

                    }
                    if (file.mass[y, x] == 'S')
                    {

                        sprite[20].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[20]);

                    }
                    if (file.mass[y, x] == 'U')
                    {

                        sprite[21].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[21]);

                    }
                    if (file.mass[y, x] == 'K')
                    {

                        sprite[23].Position = new Vector2f(x * 32, y * 32);
                        window.Draw(sprite[23]);

                    }

                }
            }
        }
    }
}
