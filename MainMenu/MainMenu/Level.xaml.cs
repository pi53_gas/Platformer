﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SFML.Graphics;
namespace MainMenu
{
    /// <summary>
    /// Логика взаимодействия для Level.xaml
    /// </summary>
    public partial class Level : Window
    {
        public Level()
        {
            InitializeComponent();
        }

        Creation.Window window = new Creation.Window();
        private void Level2_Click(object sender, RoutedEventArgs e)
        {
           
            window.Level1(900, 600, "Level1", SFML.Graphics.Color.Transparent);
            if (window.personage.IsTouch == true)
            {
                End end = new End();
                end.Owner = this;
                end.Show();

            }

        }

        private void Level3_Click(object sender, RoutedEventArgs e)
        {
            
            window.Level2(900, 600, "Level2", SFML.Graphics.Color.Transparent);
            if (window.personage.IsTouch == true)
            {
                End end = new End();
                end.Owner = this;
                end.Show();

            }
        }
    }
}
