﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Creation
{
    public class Files
    {
        public char[,] mass;
        public String FileName { set; get; }
        public Files(String FileName)
        {
            this.FileName = FileName;
        }

        public void Get()
        {
            string s;

            using (StreamReader reader = new StreamReader(FileName, Encoding.Default))
            {
                mass = new char[Convert.ToInt32(reader.ReadLine()), Convert.ToInt32(reader.ReadLine())];
                for (int j = 0; j < mass.GetLength(0); j++)
                {
                    s = reader.ReadLine();
                    // s.Split(' ');
                    for (int i = 0; i < mass.GetLength(1); i++)
                    {
                        mass[j, i] = s[i];
                       
                    }
                   
                }
            }
        }
        public void SaveToFile(MainPersonage personage, Score score)
        {
            
                using (StreamWriter result = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "result.txt"), true, Encoding.UTF8))
                {
                    result.WriteLine(score.Coin + "|" + score.Pumpkin + "|" + score.Moon + "|" + score.Ghost + "|" + score.Bonus + "\n");
                    result.Close();
                }
            


        }


    }
}
