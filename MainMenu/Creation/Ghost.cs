﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
namespace Creation
{
    class Ghost:Decoration
    {
        public bool Life { set; get; }
        public Ghost(float X, float Y, String FileName) : base(X, Y, FileName)
        {
            Life = true;
        }
        public override void Draw(RenderWindow window)
        {
            window.Draw(sprite);
        }
    }
}
