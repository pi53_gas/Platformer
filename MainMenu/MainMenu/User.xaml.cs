﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using SFML.Audio;
namespace MainMenu
{
    /// <summary>
    /// Логика взаимодействия для User.xaml
    /// </summary>
    public partial class User : Window
    {
        public User()
        {
            InitializeComponent();
        }

        private void Highscore_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }
        private void Load()
        {
           
            var ScoreList = new List<Creation.Score>();
            using (var reader = new StreamReader(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "result.txt"), Encoding.UTF8))
            {
                string Line;
                while(!string.IsNullOrWhiteSpace(Line = reader.ReadLine()))
                {
                    var Items = Line.Split('|');
                    var Element = new Creation.Score
                    {
                        Coin = int.Parse(Items[0]),
                        Pumpkin = int.Parse(Items[1]),
                        Moon = int.Parse(Items[2]),
                        Ghost = int.Parse(Items[3]),
                        Bonus = int.Parse(Items[4])


                    };
                    ScoreList.Add(Element);
                }
            }
           
                Data.ItemsSource = ScoreList;
                
            

        }
    }
}
