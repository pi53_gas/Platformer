﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
namespace Creation
{
    public abstract class Personage
    {
        public enum Direction { Stand, Left, Right, Up, Down, Jump, RightUp };
        public Direction State;
        public float X { protected set; get; }
        public float Y { protected set; get; }
        public float XCoordinate { protected set; get; }
        public float YCoordinate { protected set; get; }
        public float Width { private set; get; }
        public float Height { private set; get; }
        public float Dx { set; get; }
        public float Dy { set; get; }
        public float Speed { set; get; }
        public int Health { set; get; }
        public bool Life {  set; get; }
        public bool OnGround { protected set; get; }
        public bool IsMove { protected set; get; }
        public float Timer { private set; get; }
        public Texture texture;
        public Sprite sprite;
        public String Name { private set; get; }
        public float Animation { protected set; get; }
        public float Coins { protected set; get; }

        public Personage(float XCoordinate, float YCoordinate, float X, float Y, float Width, float Height, String Name, String FileName)
        {
            this.XCoordinate = XCoordinate;
            this.YCoordinate = YCoordinate;
            this.X = X;
            this.Y = Y;
            this.Width = Width;
            this.Height = Height;
            this.Name = Name;
            Timer = 0;
            Speed = 0;
            Health = 560;
            Dx = 0;
            Dy = 0;
            Life = true;
            OnGround = false;
            IsMove = false;
            texture = new Texture(FileName);
            sprite = new Sprite(texture);
            Animation = 0;

        }

        public abstract void Draw(RenderWindow window);
        public abstract void Update(float time, Files file);
    }
}
