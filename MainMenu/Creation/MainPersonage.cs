﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;
using SFML.System;
namespace Creation
{
   public class MainPersonage:Personage
    {
      
        public bool IsSpell { set; get; }
        public bool IsTouch { set; get; }
        public bool IsTime { set; get; }
        public MainPersonage(float XCoordinate, float YCoordinate, float X, float Y, float Width, float Height, String Name, String FileName) : base(XCoordinate, YCoordinate, X, Y, Width, Height, Name, FileName)
        {
            IsSpell = false;
            State = Direction.Stand;
            if (Name == "Sam")
            {
                sprite.TextureRect = new IntRect((int)XCoordinate, (int)YCoordinate, (int)Width, (int)Height);

            }
            IsTouch = false;
            IsTime = false;
        }
        public void Motion()
        {


            if (Life == true)
            {
                if (Keyboard.IsKeyPressed(Keyboard.Key.Left) || Keyboard.IsKeyPressed(Keyboard.Key.D))
                {
                    State = Direction.Left;
                    Speed = 0.1f;
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Right) || Keyboard.IsKeyPressed(Keyboard.Key.A))
                {
                    State = Direction.Right;
                    Speed = 0.1f;
                }
                if ((Keyboard.IsKeyPressed(Keyboard.Key.Up)|| Keyboard.IsKeyPressed(Keyboard.Key.W)) && (OnGround))
                {
                    State = Direction.Jump;
                    Dy = -0.55f;
                    OnGround = false;
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Down) || Keyboard.IsKeyPressed(Keyboard.Key.S))
                {
                    State = Direction.Down;

                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Space))
                {
                    IsSpell = true;

                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.Up) && Keyboard.IsKeyPressed(Keyboard.Key.Right))
                {
                    State = Direction.RightUp;

                }
            }
            
        }
        public override void Update(float time, Files file)
        {
            Motion();
            switch (State)
            {
                case Direction.Left: Dx = -Speed; break;
                case Direction.Right: Dx = Speed; break;
                case Direction.Up: break;
                case Direction.Down: Dx = 0; break;
                case Direction.Jump: break;
                case Direction.Stand: Dx = 0; Dy = 0; break;
                case Direction.RightUp: Dx = Speed; break;

            }
            X = X + (Dx * time);
            Interection(file, Dx, 0);
            Y = Y + (Dy * time);
            Interection(file, 0, Dy);
            sprite.Position = new Vector2f(X, Y);
            if (IsMove == false)
                Speed = 0;

            if (Health <= 0)
            {
                Life = false;
                for(int i=0; i<file.mass.GetLength(1); i++)
                {
                    Array.Clear(file.mass, 0, file.mass.GetLength(1));
                }
            }
            Dy = Dy + 0.0015f * time;
            Interection(file, 0, Dy);

            Animation = Animation + (time * 0.005f);
            if (Animation > 4)
                Animation = 0;
            if (Dx > 0)
                sprite.TextureRect = new IntRect((int)Width * (int)Animation, (int)Height * 2, (int)Width, (int)Height);
            if (Dx < 0)
                sprite.TextureRect = new IntRect((int)Width * (int)Animation, (int)Height, (int)Width, (int)Height);
            if (Dy < 0 && Dx > 0)
                sprite.TextureRect = new IntRect(0, (int)Height * 2, (int)Width, (int)Height);
            if (Dy < 0 && Dx < 0)
                sprite.TextureRect = new IntRect(0, (int)Height, (int)Width, (int)Height);

        }

        public void Interection(Files file, float dx, float dy)
        {

            for (int i = (int)Y / 32; i < ((int)Y + Height) / 32; i++)
            {
                for (int j = (int)X / 32; j < ((int)X + Width) / 32; j++)
                {

                    if (file.mass[i, j] == '0' || file.mass[i, j] == '2' || file.mass[i, j] == '3' || file.mass[i, j] == '6' || file.mass[i, j] == 's' || file.mass[i, j] == 'm' || file.mass[i, j] == 't' || file.mass[i, j] == '1' || file.mass[i, j] == '4' || file.mass[i, j] == '7' || file.mass[i, j] == 'O' || file.mass[i, j] == 'x' || file.mass[i, j] == 'y' || file.mass[i, j] == '9' || file.mass[i, j] == 'K' || file.mass[i, j] == 'U')
                    {
                        if (dy > 0)
                        {
                            Y = i * 32 - Height;
                            Dy = 0;
                            OnGround = true;
                        }
                        if (dy < 0)
                        {
                            Y = i * 32 + 32;
                            Dy = 0;
                        }
                        if (dx > 0)
                        {
                            X = j * 32 - Width;
                            Dx = 0;
                        }
                        if (dx < 0)
                        {
                            X = j * 32 + 32;
                            Dx = 0;
                        }

                    }
                    if (file.mass[i, j] == 'U')
                    {
                        Life = false;
                        State = Direction.Stand;
                        //Speed = 0;
                        IsMove = false;
                    }

                }
            }
        }

        public override void Draw(RenderWindow window)
        {
            window.Draw(sprite);
        }
    }
}
