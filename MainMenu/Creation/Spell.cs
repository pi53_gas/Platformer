﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
namespace Creation
{
    class Spell:Personage
    {
        public Direction Router;
        public Spell(float XCoordinate, float YCoordinate, float X, float Y, float Width, float Height, String Name, String FileName, Direction direction) : base(XCoordinate, YCoordinate, X, Y, Width, Height, Name, FileName)
        {

            this.Router = direction;
            Speed = 0.8f;

        }
        public override void Update(float time, Files file)
        {
            switch (Router)
            {
                case Direction.Left: Dx = -Speed; Dy = 0; break;
                case Direction.Right: Dx = Speed; Dy = 0; break;
                case Direction.Up: Dx = 0; Dy = -Speed; break;
                case Direction.Down:; break;
                case Direction.Jump: Dx = -Speed; Dy = 0 ; break;
                case Direction.Stand: Dx = Speed; break;
                case Direction.RightUp:Dx = Speed; Dy = -Speed ; break;
            }

            X = X + (Dx * time);
            Y = Y + (Dy * time);

            if (X <= 0) //Задержка пули в левой стене
                X = 1;
            if (Y <= 0)
                Y = 1;
            if (Health <= 0)
                Life = false;

            sprite.Position = new Vector2f(X, Y);
        }

        public override void Draw(RenderWindow window)
        {
            window.Draw(sprite);
        }
    }
}
