﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
namespace Creation
{
    class Roster
    {
        string Path = "..\\Decoration\\";
        string Path2 = "..\\Decoration2\\";
        public List<Adversary> adversary = new List<Adversary>();
        public List<Spell> spell = new List<Spell>();
        public List<Decoration> decoration = new List<Decoration>();
        public List<Ghost> ghost = new List<Ghost>();
        public List<Moon> moon = new List<Moon>();
        public List<Heart> heart = new List<Heart>();
        public List<Coin> coin = new List<Coin>();
        public List<Pumpkin> pumpkin = new List<Pumpkin>();
        public List<Hanako> hanako = new List<Hanako>();
        public void AddToListEnemies()
        {
            adversary.Add(new Adversary(0, 96, 380, 1100, 32, 48, "Enemy", "seychelles.png"));
            adversary.Add(new Adversary(0, 120, 720, 1060, 40, 60, "Enemy", "sadako.png"));
            adversary.Add(new Adversary(0, 130, 800, 670, 40, 65, "Enemy", "ray_noir.png"));
            adversary.Add(new Adversary(0, 146, 500, 530, 45, 73, "Enemy", "12sprite (1).png"));
            adversary.Add(new Adversary(0, 120, 2280, 455, 40, 60, "Enemy", "hungary_clubs.png"));
            adversary.Add(new Adversary(0, 134, 2720, 730, 50, 67, "Enemy", "ryuk.png"));
        }
        public void AddToListDecoration()
        {
            decoration.Add(new Decoration(50, 1027, Path + "Bush (1).png"));
            decoration.Add(new Decoration(400, 1110, Path + "Bush (2).png"));
            decoration.Add(new Decoration(380, 1130, Path + "Skeleton.png"));
            decoration.Add(new Decoration(480, 1027, Path + "Bush (1).png"));
            decoration.Add(new Decoration(500, 1000, Path + "TombStone (2).png"));
            decoration.Add(new Decoration(530, 1000, Path + "DeadBush.png"));
            decoration.Add(new Decoration(590, 1030, Path + "Skeleton.png"));
            decoration.Add(new Decoration(710, 1078, Path + "Bush (2).png"));
            decoration.Add(new Decoration(730, 1077, Path + "TombStone (2).png"));
            decoration.Add(new Decoration(760, 1097, Path + "Skeleton.png"));
            decoration.Add(new Decoration(100, 730, Path + "Tree.png"));
            decoration.Add(new Decoration(180, 840, Path + "Bush (1).png"));
            decoration.Add(new Decoration(1000, 1027, Path + "Bush (1).png"));
            decoration.Add(new Decoration(1030, 1032, Path + "Skeleton.png"));
            decoration.Add(new Decoration(1280, 937, Path + "DeadBush.png"));
            decoration.Add(new Decoration(1480, 887, Path + "Bush (2).png"));
            decoration.Add(new Decoration(1510, 905, Path + "Skeleton.png"));
            decoration.Add(new Decoration(1180, 770, Path + "Bush (1).png"));
            decoration.Add(new Decoration(990, 710, Path + "Bush (1).png"));
            decoration.Add(new Decoration(985, 680, Path + "TombStone (2).png"));
            decoration.Add(new Decoration(800, 710, Path + "Bush (2).png"));
            decoration.Add(new Decoration(840, 710, Path + "Skeleton.png"));
            decoration.Add(new Decoration(610, 535, Path + "Tree.png"));
            decoration.Add(new Decoration(610, 632, Path + "Bush (2).png"));
            decoration.Add(new Decoration(430, 580, Path + "Bush (1).png"));
            decoration.Add(new Decoration(410, 552, Path + "TombStone (2).png"));
            decoration.Add(new Decoration(455, 585, Path + "Skeleton.png"));
            decoration.Add(new Decoration(1640, 835, Path + "Bush (1).png"));
            decoration.Add(new Decoration(1840, 760, Path + "Bush (2).png"));
            decoration.Add(new Decoration(1870, 745, Path + "TombStone (2).png"));
            decoration.Add(new Decoration(2080, 740, Path + "Bush (1).png"));
            decoration.Add(new Decoration(2040, 715, Path + "DeadBush.png"));
            decoration.Add(new Decoration(2100, 745, Path + "Skeleton.png"));
            decoration.Add(new Decoration(2320, 793, Path + "Bush (2).png"));
            decoration.Add(new Decoration(2600, 835, Path + "Bush (1).png"));
            decoration.Add(new Decoration(2570, 840, Path + "Skeleton.png"));
            decoration.Add(new Decoration(2780, 770, Path + "Bush (1).png"));
            decoration.Add(new Decoration(2800, 663, Path + "Tree.png"));
            decoration.Add(new Decoration(2760, 745, Path + "TombStone (2).png"));
            decoration.Add(new Decoration(2300, 480, Path + "Bush (2).png"));
            decoration.Add(new Decoration(2280, 460, Path + "TombStone (2).png"));
            decoration.Add(new Decoration(2330, 490, Path + "Skeleton.png"));
            decoration.Add(new Decoration(3100, 705, Path + "Sign.png"));
        }
       
        public void AddToLevel2()
        {
            decoration.Add(new Decoration(50, 920, Path2 + "Tree_1.png"));
            decoration.Add(new Decoration(160, 780, Path2 + "SnowMan.png"));
            decoration.Add(new Decoration(210, 820, Path2 + "Crystal.png"));
            decoration.Add(new Decoration(420, 1105, Path2 + "Crystal.png"));
            decoration.Add(new Decoration(580, 970, Path2 + "SnowMan2.png"));
            decoration.Add(new Decoration(540, 1033, Path2 + "Stone.png"));
            decoration.Add(new Decoration(710, 1075, Path2 + "Crystal2.png"));
            decoration.Add(new Decoration(1350, 945, Path2 + "Crystal.png"));
            decoration.Add(new Decoration(1000, 650, Path2 + "SnowMan.png"));
            decoration.Add(new Decoration(1200, 775, Path2 + "Stone.png"));
            decoration.Add(new Decoration(770, 690, Path2 + "Crystal2.png"));
            decoration.Add(new Decoration(630, 500, Path2 + "Tree_2.png"));
            decoration.Add(new Decoration(2090, 683, Path2 + "SnowMan2.png"));
            decoration.Add(new Decoration(2780, 660, Path2 + "Tree_1.png"));
        }
        public void AddToListHanako()      
        {
            hanako.Add(new Hanako(40, 60, 3080, 675, "prussia_joker.png"));
        }
        public void AddToListGhost()
        {                        
            ghost.Add(new Ghost(730, 1000, Path + "large.png"));
            ghost.Add(new Ghost(675, 610, Path + "g.png"));
            ghost.Add(new Ghost(2580, 800, Path + "superthumb.png"));

        }
        public void AddToListMoon()
        {
            moon.Add(new Moon(380, 1045, Path + "moon.png"));
            moon.Add(new Moon(1170, 700, Path + "moon.png"));
            moon.Add(new Moon(1670, 780, Path + "moon.png"));
            moon.Add(new Moon(2580, 510, Path + "moon.png"));
        }

        public void AddToListHeart()
        {
            heart.Add(new Heart(350, 560, Path + "hud_heartFull.png"));
            heart.Add(new Heart(2233, 430, Path + "hud_heartFull.png"));
        }
        public void AddToCoin()
        {
            coin.Add(new Coin(580, 1000, Path + "coinGold.png"));
            coin.Add(new Coin(1900, 750, Path + "coinGold.png"));
            coin.Add(new Coin(2380, 780, Path + "coinGold.png"));
        }

        public void AddToPumpkin()
        {
            pumpkin.Add(new Pumpkin(1300, 815, Path + "Happy-Pumpkin-PNG-Free-Download.png"));
            pumpkin.Add(new Pumpkin(2260, 655, Path + "Happy-Pumpkin-PNG-Free-Download.png"));
        }
        public void S(float time, Files file, RenderWindow window)
        {
            for (int i = 0; i < spell.Count; i++)
            {
                spell[i].Update(time, file);
                spell[i].Draw(window);
                if (spell[i].Life == false)
                {
                    spell.RemoveAt(i);
                }


            }

        }
        public void DrawHanako(RenderWindow window)
        {
            for (int i = 0; i < hanako.Count; i++)
            {
                hanako[i].Draw(window);
            }
        }
        public void Decor(RenderWindow window)
        {
            for (int i = 0; i < decoration.Count; i++)
            {
                decoration[i].Draw(window);
            }
        }
        public void Gh(RenderWindow window)
        {
            for (int i = 0; i < ghost.Count; i++)
            {
                ghost[i].Draw(window);
                if (ghost[i].Life == false)
                {
                    ghost.RemoveAt(i);
                }
            }
        }
        public void Mn(RenderWindow window)
        {
            for (int i = 0; i < moon.Count; i++)
            {
                moon[i].Draw(window);
                if (moon[i].Life == false)
                {
                    moon.RemoveAt(i);
                }
            }
        }
        public void Hr(RenderWindow window)
        {
            for (int i = 0; i < heart.Count; i++)
            {
                heart[i].Draw(window);
                if (heart[i].Life == false)
                {
                    heart.RemoveAt(i);
                }
            }
        }

        public void Cn(RenderWindow window)
        {
            for (int i = 0; i < coin.Count; i++)
            {
                coin[i].Draw(window);
                if (coin[i].Life == false)
                {
                    coin.RemoveAt(i);
                }
            }
        }
        public void Pm(RenderWindow window)
        {
            for (int i = 0; i < pumpkin.Count; i++)
            {
                pumpkin[i].Draw(window);
                if (pumpkin[i].Life == false)
                {
                    pumpkin.RemoveAt(i);
                }
            }
        }
        public void DrawEnemies(float time, Files file,  RenderWindow window)
        {
            for (int i = 0; i < adversary.Count; i++)
            {
                adversary[i].Update(time, file);
                adversary[i].Draw(window);
                if (adversary[i].Life == false)
                {
                    adversary.RemoveAt(i);
                }


            }
        }
        public void Enemies(MainPersonage personage)
        {
            

            foreach (Adversary i in adversary)
            {
                foreach (Spell j in spell)
                {
                   
                    if (i.sprite.GetGlobalBounds().Intersects(j.sprite.GetGlobalBounds()))
                    {
                        i.Health = 0;
                        j.Health = 0;
                    }
                }


            }
            foreach(Adversary i in adversary)
            {
                if (i.sprite.GetGlobalBounds().Intersects(personage.sprite.GetGlobalBounds()))
                {
                    if (i.Name == "Enemy")
                    {
                        if (personage.Dy > 0 && personage.OnGround == false)
                        {
                            i.Dx = 0;
                            personage.Dy = -0.3f;
                            i.Health = 0;
                        }
                        else
                        {

                            personage.Health = personage.Health - 40;
                            Console.WriteLine(personage.Health);
                        }
                    }
                }
            }
        }
       

        public void Take(MainPersonage personage, Score score)
        {
            foreach (Ghost i in ghost)
            {
                if (i.sprite.GetGlobalBounds().Intersects(personage.sprite.GetGlobalBounds()))
                {
                    i.Life = false;
                    score.Ghost++;
                }
            }

            foreach (Moon i in moon)
            {
                if (i.sprite.GetGlobalBounds().Intersects(personage.sprite.GetGlobalBounds()))
                {
                    i.Life = false;
                    
                    score.Moon++;
                }
            }

            foreach (Heart i in heart)
            {
                if (i.sprite.GetGlobalBounds().Intersects(personage.sprite.GetGlobalBounds()))
                {
                    i.Life = false;
                  
                    score.Bonus++;
                    personage.Health += 20;
                }
            }

            foreach (Coin i in coin)
            {
                if (i.sprite.GetGlobalBounds().Intersects(personage.sprite.GetGlobalBounds()))
                {
                    i.Life = false;
                    
                    score.Coin++;
                }
            }

            foreach (Pumpkin i in pumpkin)
            {
                if (i.sprite.GetGlobalBounds().Intersects(personage.sprite.GetGlobalBounds()))
                {
                    i.Life = false;
                    score.Pumpkin++;
                   
                }
            }

            foreach(Hanako i in hanako)
            {
                if (i.sprite.GetGlobalBounds().Intersects(personage.sprite.GetGlobalBounds()))
                {
                    personage.IsTouch = true;
                    personage.Life = false;
                }
            }
        }
    }
}
