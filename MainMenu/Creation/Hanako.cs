﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
namespace Creation
{
    class Hanako:Decoration
    {
        public float Xrect { protected set; get; }
        public float Yrect { protected set; get; }

        public Hanako(float Xrect,float Yrect, float X, float Y, String FileName):base(X,Y,FileName)
        {
            this.Xrect = Xrect;
            this.Yrect = Yrect;
            sprite.TextureRect = new IntRect(0,0,(int)Xrect,(int)Yrect);
        }
        public override void Draw(RenderWindow window)
        {
            window.Draw(sprite);
        }
    }
}
