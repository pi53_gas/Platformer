﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
namespace Creation
{
    class Adversary:Personage
    {
        public Adversary(float XCoordinate, float YCoordinate, float X, float Y, float Width, float Height, String Name, String FileName) : base(XCoordinate, YCoordinate, X, Y, Width, Height, Name, FileName)
        {
            if (Name == "Enemy")
            {
                sprite.TextureRect = new IntRect((int)XCoordinate, (int)YCoordinate, (int)Width, (int)Height);

            }
            Dx = 0.1f;
        }

        public override void Draw(RenderWindow window)
        {
            window.Draw(sprite);
        }

        public void Interection(Files file, float dx, float dy)
        {

            for (int i = (int)Y / 32; i < ((int)Y + Height) / 32; i++)
            {
                for (int j = (int)X / 32; j < ((int)X + Width) / 32; j++)
                {
                    if (file.mass[i, j] == '0'|| file.mass[i, j] == 'j' || file.mass[i, j] == 'K' || file.mass[i, j] == '2' || file.mass[i, j] == '6' || file.mass[i, j] == '4' )
                    {
                        if (dy > 0)
                        {
                            Y = i * 32 - Height;
                            //Dy = 0;
                        }
                        if (dy < 0)
                        {
                            Y = i * 32 + 32;
                            //Dy = 0;
                        }
                        if (dx > 0)
                        {
                            X = j * 32 - Width;
                            Dx = -0.1f;
                            sprite.TextureRect = new IntRect(0, (int)Height, (int)Width, (int)Height);
                        }
                        if (dx < 0)
                        {
                            X = j * 32 + 32;
                            Dx = 0.1f;
                            sprite.TextureRect = new IntRect(0, (int)Height * 2, (int)Width, (int)Height);
                        }
                    }


                }
            }
        }

        public override void Update(float time, Files file)
        {
            Dy = Dy + 0.0015f * time;
            Interection(file, Dx, 0);
            X = X + (Dx * time);
            sprite.Position = new Vector2f(X, Y);
            if (Health <= 0)
                Life = false;

            Animation = Animation + (time * 0.005f);
            if (Animation > 4)
                Animation = 0;
            if (Dx > 0)
                sprite.TextureRect = new IntRect((int)Width * (int)Animation, (int)Height * 2, (int)Width, (int)Height);
            if (Dx < 0)
                sprite.TextureRect = new IntRect((int)Width * (int)Animation, (int)Height, (int)Width, (int)Height);



        }
    }
}
